package com.example.thymeleafdemo.persistence.repository;

import com.example.thymeleafdemo.persistence.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
