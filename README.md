#### Demo Thymeleaf application

Usage:
start application using **mvn clean compile spring-boot:run**

Architecture: 3-Tier
* controller
* persistence
* service

Tools:

* Flyway
  * **V#__script.sql** files for data model creation
  * **R__insert_test_data.sql** file for test data population
* Thymeleaf templates
  * src/main/resources/templates html templates
  * src/main/resources/static/css/main.css style sheet 
